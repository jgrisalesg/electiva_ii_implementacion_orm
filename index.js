const express = require('express');
const mongoose = require('mongoose');
const app = express();
require('dotenv').config();
const port = process.env.PORT;
const routerApi = require('./src/routes');
const {errorhandler, logErrors} = require('./src/middlewares/error.handler')

app.listen(port, () => console.log('Connect in the port ', port));

mongoose
  .connect(process.env.MONGODB_CONNECTION_STRING)
  .then(() => console.log('Success connect with mongoDB'))
  .catch((error) => console.log(error));
/* Respuestas en formato JSON */
app.use(express.json());
app.use(logErrors);
app.use(errorhandler);
routerApi(app);
